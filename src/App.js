import React, { useState } from 'react';
import styled from '@emotion/styled';
import Dropdown from './Components/Dropdown';

const Background = styled.div`
  background: ${p => `${p.pickedBackground};`};
  height: 100%;
  width: 100%;
  position: absolute;
  transition: all 0.4s ease;

  * {
    box-sizing: border-box;
  }
`;

const App = () => {
  const [color, setColor] = useState('slategray');
  const handleBackgroundColor = color => {
    setColor(color);
  };
  return (
    <Background pickedBackground={color}>
      <Dropdown handleBackgroundColor={handleBackgroundColor} />
    </Background>
  );
};

export default App;
