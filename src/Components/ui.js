import React from 'react';
import styled from '@emotion/styled';

export const SearchInput = styled.input`
  margin: 0;
  padding: 0.2rem 1rem;
  width: 100%;
  font-size: 2.5rem;
  outline: 0;
  border: 1px solid #efefef;
  background: #fdfcfc;
  border-radius: 4px 4px 0 0;
  box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.1);
`;

const ToggleButton = styled.button`
  cursor: pointer;
  background: transparent;
  outline: 0;
  border: 0;
  padding: 0;
  margin: 0;
  font-size: 4rem;
  position: relative;
  position: absolute;
  right: 0;

  svg {
    transform: ${p => p.isOpen && 'rotate(-.5turn);'};
    transition: all 0.4s ease;
  }
`;

export const ToggleColors = ({ children, isOpen }) => (
  <ToggleButton isOpen={isOpen}>{children}</ToggleButton>
);

export const Colors = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
  background: #fdfcfc;
  position: absolute;
  max-height: 340px;
  overflow: ${p => (p.isOpen ? 'scroll' : 'hidden')};
  width: 100%;
  border-radius: 0 0 4px 4px;
  box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2);
  position: absolute;

  .color-enter {
    opacity: 0;
  }
  .color-enter-active {
    opacity: 1;
    transition: opacity 500ms ease-in;
  }
  .color-exit {
    opacity: 1;
  }
  .color-exit-active {
    opacity: 0;
    transition: opacity 500ms ease-in;
  }
`;

export const Color = styled.li`
  padding: 0.5rem 1rem;
  line-height: 2rem;
  cursor: pointer;
  font-size: 1.5rem;
  background: ${p => p.isHighlighted && 'lightpink'};
`;
