import React, { useState, useEffect } from 'react';
import styled from '@emotion/styled';
import Downshift from 'downshift';
import colors from 'color-name';
import { SearchInput, ToggleColors, Colors, Color } from './ui';
import { MdKeyboardArrowUp as Arrow } from 'react-icons/md';

const DropdownWrapper = styled.div`
  position: absolute;
  top: 40%;
  left: 50%;
  transform: translate(-50%, -60%);
  max-width: 420px;
  > div {
    position: relative;
  }
`;

const colorNames = obj => {
  const colorWithCode = Object.entries(obj).map(([name, code]) => {
    // destructuring name, code and creating an array of objects
    // like so: { name: aliceblue, code: [255,255,0] }
    return {
      name,
      code: code.toString()
    };
  });
  return colorWithCode;
};

const Dropdown = ({ handleBackgroundColor }) => {
  const [value, handleInputValue] = useState('slategray');
  useEffect(() => {});
  return (
    <DropdownWrapper>
      <Downshift
        initialInputValue={'slategray'}
        onChange={selected => {
          handleBackgroundColor(selected);
          handleInputValue('');
        }}
      >
        {({
          getInputProps,
          getItemProps,
          getMenuProps,
          getToggleButtonProps,
          isOpen,
          highlightedIndex,
          inputValue
        }) => (
          <div>
            <SearchInput
              {...getInputProps({
                type: 'text',
                name: 'color',
                value: inputValue,
                onChange: e => handleInputValue(e.target.value)
              })}
            />
            <ToggleColors isOpen={isOpen}>
              <Arrow {...getToggleButtonProps()} />
            </ToggleColors>
            <Colors {...getMenuProps()} isOpen={isOpen}>
              {isOpen &&
                colorNames(colors).map((color, i) => (
                  <Color
                    key={i}
                    {...getItemProps({
                      index: i,
                      item: color.name,
                      code: color.code,
                      isHighlighted: highlightedIndex === i
                    })}
                  >
                    {color.name}
                  </Color>
                ))}
            </Colors>
          </div>
        )}
      </Downshift>
    </DropdownWrapper>
  );
};

export default Dropdown;

// .filter(colors => {
//   const searchTerm = new RegExp(inputValue, 'i');
//   return colors.name.match(searchTerm);
// })
